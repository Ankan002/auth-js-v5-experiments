"use server";

import { signIn } from "@/auth";

export const onClick = async () => {
	await signIn();
};
