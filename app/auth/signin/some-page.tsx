"use client";

import { signIn, signOut } from "next-auth/react";
import { useEffect } from "react";

export const SomePage = () => {
	const getSomething = async () => {
		const res = await fetch("/api/random", {
			method: "GET",
			headers: {
				"content-type": "application/json",
			},
		});

		const d = await res.json();

		console.log(d);
	};

	const onClick = () => {
		signIn("google");
	};

	const onS = () => {
		signOut();
	};

	useEffect(() => {
		getSomething();
	}, []);

	return (
		<main className="min-h-screen w-full flex flex-col items-center justify-center bg-white">
			<button
				className="p-2 text-lg font-semibold text-black"
				onClick={onClick}
			>
				Google
			</button>

			<button
				className="p-2 text-lg font-semibold text-black"
				onClick={onS}
			>
				Sign Out
			</button>
		</main>
	);
};
