import { auth } from "@/auth";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
	const r = await auth();

	// console.log(r);

	return NextResponse.json(
		{
			data: r?.user,
		},
		{
			status: 200,
		}
	);
};

export const revalidate = 0;
