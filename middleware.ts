import { auth } from "@/auth";
import { NextResponse } from "next/server";

export default auth((request) => {
	const a = request.auth;

	const url = request.nextUrl.pathname;

	if (url === "/auth/signin") {
		if (a) return NextResponse.redirect("http://localhost:3000");
		return NextResponse.next();
	}

	return NextResponse.next();
});
