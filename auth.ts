import NextAuth from "next-auth";
import Google from "next-auth/providers/google";

export const {
	auth,
	handlers: { GET, POST },
	signIn,
	signOut,
} = NextAuth({
	providers: [
		Google({
			clientId: process.env["AUTH_GOOGLE_CLIENT_ID"],
			clientSecret: process.env["AUTH_GOOGLE_CLIENT_SECRET"],
		}),
	],
	callbacks: {
		signIn: ({ account, user, credentials, email, profile }) => {
			// console.log({
			// 	account,
			// 	user,
			// 	credentials,
			// 	email,
			// 	profile,
			// });

			// console.log("SI");

			user.id = account?.providerAccountId;

			return true;
		},
		session(params) {
			// console.log({ user: params.user });
			params.session.user.id = params.token.sub ?? "";
			// console.log({ session: params.session });

			// console.log({ userRecord: params.user });
			// console.log({ token: params.token });

			return params.session;
		},
		jwt(params) {
			// console.log(params);
			// params["user"]["id"] = params.account?.providerAccountId;

			// console.log("To", Date.now());

			params.token.sub = "11";

			return params.token;
		},
		redirect(params) {
			return params.baseUrl;
		},
	},
	session: {
		strategy: "jwt",
	},
	pages: {
		signIn: "/auth/signin",
	},
});
